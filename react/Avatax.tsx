import React from 'react'
import styled from 'styled-components'
import axios, { AxiosResponse } from 'axios'

import { useProduct } from 'vtex.product-context'

const Avatax: StorefrontFunctionComponent = () => {
  const productContextValue = useProduct()

  let establishment = {
    street: '',
    neighborhood: '',
    zipCode: '',
    cityCode: '',
    cityName: '',
    state: '',
    countryCode: '',
    country: '',
    number: '',
    complement: '',
    phone: '',
    suframa: '',
    cnpj: ''
  }

  let client = {
    street: '',
    neighborhood: '',
    zipCode: '',
    cityCode: '',
    cityName: '',
    state: '',
    countryCode: '',
    country: '',
    number: '',
    complement: '',
    phone: '',
    suframa: '',
    cnpj: ''
  }

  let calculatedTaxes = {}
  
  const getTaxData = () => {
    axios.get('/api/dataentities/AE/documents/0aa625b6-3e4b-11eb-82ac-0e87f244e709?search&_fields=clientId,clientSecret,street,neighborhood,zipCode,cityCode,city,state,country,streetNumber,complement,phone,cnpj,suframa&_sort=clientId ASC', {
      headers: {
        Accept: 'application/vnd.vtex.ds.v10+json',
        'Content-Type': 'application/json'
      }
    }).then((response: AxiosResponse) => {
      const credentials = {
        grantType: 'client_credentials',
        clientId: response.data.clientId,
        clientSecret: response.data.clientSecret
      }

      establishment.street = response.data.street
      establishment.neighborhood = response.data.neighborhood
      establishment.zipCode = response.data.zipCode,
      establishment.cityCode = response.data.cityCode
      establishment.cityName = response.data.cityName
      establishment.state = response.data.state
      establishment.countryCode = response.data.countryCode
      establishment.country = response.data.country
      establishment.number = response.data.number
      establishment.complement = response.data.complement
      establishment.phone = response.data.phone
      establishment.suframa = response.data.suframa
      establishment.cnpj = response.data.cnpj

      axios.get(`/no-cache/profileSystem/getProfile`)
      .then((response: AxiosResponse) => {
        if(response.data.Email) {
          axios.get(`/api/dataentities/AD/search?_where=userId=${response.data.UserId}&_fields=city,complement,country,neighborhood,number,postalCode,state,street`)
          .then((response: AxiosResponse) => {
            if(response.data) {
              client.street = response.data.street
              client.neighborhood = response.data.neighborhood
              client.zipCode = response.data.postalCode
              client.cityCode = response.data.cityCode
              client.cityName = response.data.cityName
              client.state = response.data.state
              client.countryCode = response.data.countryCode
              client.country = response.data.country
              client.number = response.data.number
              client.complement = response.data.complement
              client.phone = response.data.phone
              client.suframa = response.data.suframa
              client.cnpj = response.data.cnpj

              axios.get(`/api/dataentities/AS/search?_where=skuId=${productContextValue?.selectedItem?.itemId}&_fields=skuId,skuInfos`)
              .then((response: AxiosResponse) => {
                const productModified = response.data[0].productInfos

                const payLoad = {
                  "header": {
                    "transactionDate": new Date().toISOString().slice(0,10),
                    "amountCalcType": "gross",
                    "documentCode": "Avatax",
                    "goods": {
                      "model": "55"
                    },
                    "messageType": "goods",
                    "eDocCreatorType": "self",
                    "eDocCreatorPerspective": true,
                    "companyLocation": "",
                    "operationType": "",
                    "locations": {
                      "establishment": {
                        "activitySector": {
                          "type": "activityLine",
                          "code": "industry"
                        },
                        "taxesSettings": {
                          "icmsTaxPayer": true,
                          "issRfRate": "",
                          "pCredSN": 0,
                          "subjectToPayrollTaxRelief": true,
                          "subjectWithholdingIss": true
                        },
                        "taxRegime": "realProfit",
                        "entityType": "business",
                        "stateTaxId": "",
                        "suframa": establishment.suframa ? establishment.suframa : 999999999,
                        "type": "business",
                        "address": {
                          "street": establishment.street,
                          "neighborhood": establishment.neighborhood,
                          "zipcode": establishment.zipCode,
                          "cityCode": establishment.cityCode,
                          "cityName": establishment.cityName,
                          "state": establishment.state,
                          "countryCode": establishment.countryCode,
                          "country": establishment.country,
                          "number": establishment.number,
                          "complement": establishment.complement,
                          "phone": establishment.phone
                        },
                        "federalTaxId": establishment.cnpj
                      },
                      "entity": {
                        "activitySector": {
                          "type": "activityLine",
                          "code": "wholesale"
                        },
                        "taxesSettings": {
                          "icmsTaxPayer": true,
                          "issRfRate": "",
                          "pCredSN": 0,
                          "subjectToPayrollTaxRelief": true,
                          "subjectWithholdingIss": true
                        },
                        "taxRegime": "estimatedProfit",
                        "entityType": "business",
                        "stateTaxId": "",
                        "suframa": 999999999,
                        "type": "business",
                        "address": {
                          "street": "Alameda dos Resedás",
                          "neighborhood": "Cerâmica",
                          "zipcode": "09531160",
                          "cityCode": "3548807",
                          "cityName": "São Caetano do Sul",
                          "state": "SP",
                          "countryCode": 0,
                          "country": "BRA",
                          "number": "",
                          "complement": "",
                          "phone": ""
                        },
                        "federalTaxId": "999999999"
                      }
                    }
                  },
                  "lines": [
                    JSON.parse(productModified)
                  ]
                }

                axios.post('https://cors-anywhere.herokuapp.com/https://api-gateway.sandbox.avalarabrasil.com.br/oauth/token', {
                  grant_type: credentials.grantType,
                  client_id: credentials.clientId,
                  client_secret: credentials.clientSecret
                })
                .then((response: AxiosResponse) => {
                  axios.post('https://avataxbr.sandbox.avalarabrasil.com.br/v3/calculations', JSON.stringify(payLoad), {
                    headers: {
                      Authorization: `bearer ${response.data.access_token}`,
                      'Content-Type': 'application/json',
                      Accept: 'application/json',
                    }
                  })
                  .then(function (response) {
                    calculatedTaxes = response.data.summary.taxByType
                  })
                  .catch(function (error) {
                    console.error(error)
                  })
                })
                .catch((error) => {
                  console.error(error)
                })
              })
              .catch((error) => {
                console.error(error)
              })
            }
          })
          .catch((error) => {
            console.error(error)
          })
        }
      })
      .catch((error) => {
        console.error(error)
      })
    })
    .catch((error) => {
      console.error(error)
    })
  }

  getTaxData()

  console.log('>>>>>>> ', calculatedTaxes)

  return (
    <Container>
      <Grid>
        <Content>
          <Paragraph>Taxas:</Paragraph>
          <Paragraph>ICMS: </Paragraph>
          <Paragraph>IPI: </Paragraph>
          <Paragraph>PIS: </Paragraph>
          <Paragraph>COFINS: </Paragraph>
        </Content>
      </Grid>
    </Container>
  )
}

const Container = styled.div`
  border: 1px solid #bbb;
  max-width: 1040px;
  margin: 20px auto;
  padding: 10px 30px;

  @media (max-width: 767.98px) {
    max-width: 100%;
    margin: 20px;
    padding: 10px;
  }

  * {
    font-family: Montserrat, sans-serif;
  }
`

const Grid = styled.div`
  display: flex;
  justify-content: space-between;

  @media (max-width: 767.98px) {
    display: block;
  }
`

const Content = styled.div``

const Paragraph = styled.p`
  font-weight: 600;
  font-size: 12px;
  color: #184077;
`

export default Avatax
