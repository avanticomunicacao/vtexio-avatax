import { ExternalClient, InstanceOptions, IOContext } from '@vtex/api'
import axios, { AxiosResponse } from 'axios'

import { Client } from '../typings/client'
import { Establishment } from '../typings/establishment'
import { Product } from '../typings/product'

import { VTEX_APP_KEY, VTEX_APP_TOKEN } from '../utils/constants'

export class TaxProvider extends ExternalClient {

  public calculatedTaxes: Object[] = []
  constructor(ctx: IOContext, options?: InstanceOptions) {
    super('https://avataxbr.avalarabrasil.com.br', ctx, options)
  }

  public async getTaxInformation(orderInformation: CheckoutRequest, account: string) {
    const allTaxes: Object[] = []

    await this.getTax(orderInformation.items, account, orderInformation).then((ct: any[]) => {
      for(let i = 0; i < ct.length; i++) {
        const productItem = {
          id: '' + i,
          taxes: [] as any,
        }
        
        for(let j = 0; j < ct[i].taxDetails.length; j++) {       
          if(ct[i].taxDetails[j].taxImpact.impactOnFinalPrice == 'Add') {
            productItem.taxes.push({
              name: ct[i].taxDetails[j].taxType,
              description: '',
              value: ct[i].taxDetails[j].tax,
            })
          }
        }

        allTaxes.push(productItem)
      }
    })

    return {
      'itemTaxResponse': allTaxes,
      'hooks': [
        {
          'major': 1,
          'url': 'https://master--bufferin.myvtex.com/app/tax-provider/oms/invoice',
        },
      ],
    }
  }

  public async getClient(account: string, orderInformation: CheckoutRequest):Promise<Client> {
    const client:Client = {
      docCode: 0,
      activitySector: '',
      icmsTaxPayer: false,
      issRfRate: '',
      pCredSN: 0,
      subjectToPayrollTaxRelief: false,
      subjectWithholdingIss: false,
      taxRegime: '',
      entityType: '',
      stateTaxId: '',
      street: '',
      neighborhood: '',
      zipCode: '',
      cityCode: '',
      cityName: '',
      type: '',
      state: '',
      countryCode: 0,
      country: '',
      number: '',
      complement: '',
      phone: '',
      suframa: '',
      cnpj: '',
      operationType: '',
      useType: '',
      usagePurpose: '',
      entityIcmsStSubstitute: false,
    }

    await axios.get(`https://${account}.vtexcommercestable.com.br/api/dataentities/CL/search?_fields=entityType,activitySector,icmsTaxPayer,type,operationType,taxRegime,useType,usagePurpose,entityIcmsStSubstitute,suframa,phone&_keyword=${orderInformation.clientData.email}`, {
      headers: {
        Accept: 'application/vnd.vtex.ds.v10+json',
        'Content-Type': 'application/json',
      },
    }).then((response: AxiosResponse) => {
      if(response.data.length) {
        client.entityType = response.data[0].entityType
        client.activitySector = response.data[0].activitySector
        client.icmsTaxPayer = (response.data[0].icmsTaxPayer == 'true')
        client.issRfRate = ''
        client.pCredSN = 0
        client.type = response.data[0].type
        client.taxRegime = response.data[0].taxRegime
        client.subjectToPayrollTaxRelief = true
        client.operationType = response.data[0].operationType
        client.useType = response.data[0].useType
        client.usagePurpose = response.data[0].usagePurpose
        client.phone = response.data[0].phone
        client.suframa = response.data[0].suframa
        client.entityIcmsStSubstitute = response.data[0].entityIcmsStSubstitute
      }
    })
    .catch((error) => {
      console.error(error)
    })

    await axios.get(`https://${account}.vtexcommercestable.com.br/api/dataentities/AD/search?_fields=userId,cityCode,number,complement&_keyword=${orderInformation.clientEmail}`, {
      headers: {
        Accept: 'application/vnd.vtex.ds.v10+json',
        'Content-Type': 'application/json',
      },
    }).then((response: AxiosResponse) => {
      if(response.data.length) {
        client.cityCode = response.data[0].cityCode
        client.number = response.data[0].number
        client.complement = response.data[0].complement
      }
    })
    .catch((error) => {
      console.error(error)
    })

    await Promise.resolve('Success').then(function() {
      client.docCode = orderInformation.orderFormId
      client.street = orderInformation.shippingDestination.street
      client.neighborhood = orderInformation.shippingDestination.neighborhood
      client.zipCode = orderInformation.shippingDestination.postalCode
      client.cityName = orderInformation.shippingDestination.city
      client.state = orderInformation.shippingDestination.state
      client.country = orderInformation.shippingDestination.country
    })

    return client
  }

  public async getEstablishment(account: string, orderInformation: CheckoutRequest):Promise<Establishment> {
    const orderFormDockId = orderInformation.items[0].dockId

    const establishment:Establishment = {
      clientId: '',
      clientSecret: '',
      clientCompanyLocation: '',
      dockId: '',
      dockName: '',
      activitySector: '',
      icmsTaxPayer: false,
      issRfRate: '',
      pCredSN: 0,
      subjectToPayrollTaxRelief: false,
      subjectWithholdingIss: false,
      taxRegime: '',
      entityType: '',
      stateTaxId: '',
      street: '',
      neighborhood: '',
      zipCode: '',
      cityCode: '',
      cityName: '',
      state: '',
      countryCode: 0,
      country: '',
      number: '',
      complement: '',
      phone: '',
      suframa: '',
      cnpj: '',
      messageType: '',
    }

    await axios.get(`https://${account}.vtexcommercestable.com.br/api/dataentities/AE/search?_fields=clientId,clientSecret,clientCompanyLocation,dockId,dockName,activitySector,icmsTaxPayer,issRfRate,pCredSN,subjectToPayrollTaxRelief,taxRegime,entityType,stateTaxId,messageType,street,neighborhood,zipCode,cityCode,city,state,country,streetNumber,complement,phone,cnpj,suframa&_keyword=${orderFormDockId}`, {
      headers: {
        Accept: 'application/vnd.vtex.ds.v10+json',
        'Content-Type': 'application/json',
      },
    }).then((response: AxiosResponse) => {
      if(response.data.length) {
        establishment.clientId = response.data[0].clientId,
        establishment.clientSecret = response.data[0].clientSecret
        establishment.clientCompanyLocation = response.data[0].clientCompanyLocation
        establishment.messageType = response.data[0].messageType
        establishment.street = response.data[0].street
        establishment.taxRegime = response.data[0].taxRegime
        establishment.activitySector = response.data[0].activitySector
        establishment.icmsTaxPayer = (response.data[0].icmsTaxPayer == 'true')
        establishment.issRfRate = response.data[0].issRfRate
        establishment.pCredSN = response.data[0].pCredSN
        establishment.subjectToPayrollTaxRelief = (response.data[0].subjectToPayrollTaxRelief == 'true')
        establishment.neighborhood = response.data[0].neighborhood
        establishment.zipCode = response.data[0].zipCode,
        establishment.cityCode = response.data[0].cityCode
        establishment.cityName = response.data[0].cityName
        establishment.state = response.data[0].state
        establishment.countryCode = response.data[0].countryCode
        establishment.country = response.data[0].country
        establishment.number = response.data[0].number
        establishment.complement = response.data[0].complement
        establishment.phone = response.data[0].phone
        establishment.suframa = response.data[0].suframa
        establishment.cnpj = response.data[0].cnpj
      }
    })
    .catch((error) => {
      console.error(error)
    })

    return establishment
  }

  public async getProducts(products: Item[], account: string, client: Client, orderInformation: CheckoutRequest):Promise<Product[]> {
    const productList: Product[] = []
    
    products.map(async product => {
      await axios.get(`https://${account}.vtexcommercestable.com.br/api/dataentities/AS/search?_where=skuId=${product.sku}&_fields=skuId,skuInfo`)
      .then((response: AxiosResponse) => {
        if(response.data[0]) {
          const prod = JSON.parse(response.data[0]?.skuInfo)
          prod.lineUnitPrice = product.itemPrice
          prod.operationType = client.operationType
          prod.useType = client.useType
          prod.usagePurpose = client.usagePurpose == null ? '' : client.usagePurpose
          prod.itemDescriptor.entityIcmsStSubstitute = client.entityIcmsStSubstitute

          orderInformation.items.map((skuItem, index) => {
            if(skuItem.sku == response.data[0].skuId) {
              prod.id = index
              prod.lineCode = index
              prod.lineAmount = skuItem.itemPrice * skuItem.quantity
              prod.numberOfItems = skuItem.quantity
              prod.freightAmount = skuItem.freightPrice
            }
          })

          productList.push(prod)
        }
      })
    })

    return productList
  }

  public async getAvalaraToken(clientId: string, clientSecret: string):Promise<string> {
    let accessToken: string = ''

    await axios.post('https://avataxbr.avalarabrasil.com.br/v3/token', {
      grant_type: 'client_credentials',
      client_id: clientId,
      client_secret: clientSecret,
      disable_token_refresh: true
    })
    .then(async (response: AxiosResponse) => {
      accessToken = response.data.access_token
    })
    .catch((error) => {
      console.error(error)
    })

    return accessToken
  }

  public async getAvalaraTax(token: string, payLoad: Object):Promise<Object[]> {
    let calculatedTaxes: Object[] = []

    await axios.post('https://avataxbr.avalarabrasil.com.br/v3/calculations', JSON.stringify(payLoad), {
      headers: {
        Authorization: `bearer ${token}`,
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
    })
    .then(async (response) => {
      calculatedTaxes = response.data.lines
    })
    .catch(function (error) {
      console.error(error)
    })

    return calculatedTaxes
  }

  public async saveLog(account: string, email: string, orderId: string, url: string, payLoad: object):Promise<Object> {
    let postResponse: Object = []

    const initLogData = {
      orderId: orderId,
      data: new Date(),
      email: email,
      url: url,
      payLoad: JSON.stringify(payLoad)
    }

    await axios.post(`https://${account}.vtexcommercestable.com.br/api/dataentities/AL/documents`, initLogData, {
      headers: {
        Accept: 'application/vnd.vtex.ds.v10+json',
			  'Content-Type': 'application/json',
        'x-vtex-api-appKey': VTEX_APP_KEY,
        'x-vtex-api-appToken': VTEX_APP_TOKEN
      },
    }).then((response: AxiosResponse) => {
      postResponse = response.data
    })
    .catch((error) => {
      console.error(error)
    })

    return postResponse
  }

  public async getTax(products: Item[], account: string, orderInformation: CheckoutRequest):Promise<Object[]> {
    let establishment: Establishment
    let client: Client
    let productList: Object[]

    await this.getEstablishment(account, orderInformation).then(async (_establishment) => {
      establishment = _establishment

      await this.getClient(account, orderInformation).then(async (_client) => {
        client = _client

        await this.getProducts(products, account, client, orderInformation).then(async (_products) => {
          productList = _products

          const payLoad:Object = {
            'header': {
              'transactionDate': new Date().toISOString().slice(0,10),
              'amountCalcType': 'gross',
              'documentCode': client.docCode,
              'goods': {
                'model': '55',
              },
              'messageType': establishment.messageType,
              'eDocCreatorType': 'self',
              'eDocCreatorPerspective': true,
              'companyLocation': establishment.clientCompanyLocation,
              'operationType': '',
              'locations': {
                'establishment': {
                  'activitySector': {
                    'type': 'activityLine',
                    'code': establishment.activitySector,
                  },
                  'taxesSettings': {
                    'icmsTaxPayer': establishment.icmsTaxPayer,
                    'issRfRate': establishment.issRfRate,
                    'pCredSN': establishment.pCredSN,
                    'subjectToPayrollTaxRelief': establishment.subjectToPayrollTaxRelief,
                    'subjectWithholdingIss': true,
                  },
                  'taxRegime': establishment.taxRegime,
                  'entityType': establishment.entityType,
                  'stateTaxId': establishment.stateTaxId,
                  'suframa': establishment.suframa ? establishment.suframa : '999999999',
                  'address': {
                    'street': establishment.street,
                    'neighborhood': establishment.neighborhood,
                    'zipcode': establishment.zipCode,
                    'cityCode': establishment.cityCode,
                    'cityName': establishment.cityName,
                    'state': establishment.state,
                    'countryCode': establishment.countryCode,
                    'country': establishment.country,
                    'number': establishment.number,
                    'complement': establishment.complement,
                    'phone': establishment.phone,
                  },
                  'federalTaxId': establishment.cnpj,
                },
                'entity': {
                  'activitySector': {
                    'type': 'activityLine',
                    'code': client.activitySector,
                  },
                  'taxesSettings': {
                    'icmsTaxPayer': client.icmsTaxPayer,
                    'issRfRate': client.issRfRate,
                    'pCredSN': client.pCredSN,
                    'subjectToPayrollTaxRelief': client.subjectToPayrollTaxRelief,
                    'subjectWithholdingIss': client.subjectWithholdingIss,
                  },
                  'taxRegime': client.taxRegime,
                  'type': client.type,
                  'entityType': client.entityType,
                  'stateTaxId': client.stateTaxId,
                  'suframa': client.suframa ? client.suframa : '999999999',
                  'inssBasisDiscount': null,
                  'inssPreviousContrib': null,
                  'address': {
                    'street': client.street,
                    'neighborhood': client.neighborhood,
                    'zipcode': client.zipCode,
                    'cityCode': client.cityCode,
                    'cityName': client.cityName,
                    'state': client.state,
                    'countryCode': client.countryCode,
                    'country': client.country,
                    'number': client.number,
                    'complement': client.complement,
                    'phone': client.phone,
                  },
                  'federalTaxId': client.cnpj,
                },
              },
            },
            'lines': productList,
          }

          await this.getAvalaraToken(establishment.clientId, establishment.clientSecret).then(async (_token) => {
            await this.getAvalaraTax(_token, payLoad).then(async (_calculatedTaxes) => {
              this.calculatedTaxes = _calculatedTaxes

              console.log(this.calculatedTaxes)

              await this.saveLog(account, orderInformation.clientEmail, client.docCode, '', payLoad).then(async (_response) => {
                let responseData = _response as any

                let patchData = {
                  email: orderInformation.clientEmail,
                  client: client,
                  establishment: establishment,
                  freightSimulation: orderInformation.totals[2].value,
                  avalaraReturn: this.calculatedTaxes,
                  pdpReturn: '',
                  type: 'Checkout'
                }

                axios.patch(`https://${account}.vtexcommercestable.com.br/api/dataentities/AL/documents/${responseData.DocumentId}`, patchData, {
                  headers: {
                    'X-VTEX-API-AppKey': VTEX_APP_KEY,
                    'X-VTEX-API-AppToken': VTEX_APP_TOKEN
                  },
                }).then((response: AxiosResponse) => {
                  if(response.data.length) {
                    console.log('Saved!')
                  }
                })
                .catch((error) => {
                  console.error(error)
                })
              })
            })
          })
        })
      })
    })

    return this.calculatedTaxes
  }
}
