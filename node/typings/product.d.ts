import { Goods } from './Goods'
import { ItemDescriptor } from './itemDescriptor'

export interface Product {
	id: any
	lineCode: any
	operationType: any
	lineAmount: any
	itemCode: any
	useType: any
	numberOfItems: any
	otherCostAmount: any
	freightAmount: any
	insuranceAmount: any
	lineTaxedDiscount: any
	lineUnitPrice: any
	subjectToIPIonInbound: any
	usagePurpose: any
	itemDescriptor: ItemDescriptor
	customProperties: any
	goods: Goods
}