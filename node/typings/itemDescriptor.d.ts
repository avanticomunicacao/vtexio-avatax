export interface ItemDescriptor {
	taxCode: any
	hsCode: any
	ex: any
	cest: any
	cean: any
	isIcmsStSubstitute: any
	source: any
	productType: any
	manufacturerEquivalent: any
	package: any
	appropriateIPIcreditWhenInGoing: any
	appropriateICMScreditWhenInGoing: any
	usuallyAppropriatePISCOFINSCredit: any
	isPisCofinsEstimatedCredit: any
	piscofinsRevenueType: any
	unitIPIfactor: any
	unitIcmsfactor: any
	unitIcmsStfactor: any
	unitPisCofinsfactor: any
	comexTaxUnitFactor: any
}