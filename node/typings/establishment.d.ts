export interface Establishment {
	clientId: any
	clientSecret: any
	clientCompanyLocation: any
	dockId: any
	dockName: any
	activitySector: any
	icmsTaxPayer: any
	issRfRate: any
	pCredSN: any
	subjectToPayrollTaxRelief: any
	subjectWithholdingIss: any
	taxRegime: any
	entityType: any
	stateTaxId: any
	suframa: any
	street: any
	neighborhood: any
	zipCode: any
	cityCode: any
	cityName: any
	state: any
	countryCode: any
	country: any
	number: any
	complement: any
	phone: any
	cnpj: any
	messageType: any
}