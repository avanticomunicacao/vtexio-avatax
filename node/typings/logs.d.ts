export interface LogResponse {
  Id: string
  Href: string
	DocumentId: string
}