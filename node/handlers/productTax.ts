import axios, { AxiosResponse } from 'axios'
import { URL } from 'url'

import { Client } from '../typings/client'
import { Establishment } from '../typings/establishment'
//import { Product } from '../typings/product'
//import TaxResponse from '../typings/taxes'

import { VTEX_APP_KEY, VTEX_APP_TOKEN } from '../utils/constants'

export async function productTax(
	ctx: Context,
	next: () => Promise<unknown>
) {
	const {
		vtex: { account },
	} = ctx

	const currentUrl = new URL(`https://${account}.vtexcommercestable.com.br${ctx.originalUrl}`)
	const search_params = currentUrl.searchParams;

	let data = {
		dockId: '',
		client: '',
		skuId: '',
		skuPrice: 0,
		freightPrice: 0
	} as any

	search_params.forEach((key, value) => {
		data[value] = key
	});

	const client: Client = {
		docCode: 0,
		activitySector: '',
		icmsTaxPayer: false,
		issRfRate: '',
		pCredSN: 0,
		subjectToPayrollTaxRelief: false,
		subjectWithholdingIss: false,
		taxRegime: '',
		entityType: '',
		stateTaxId: '',
		street: '',
		neighborhood: '',
		zipCode: '',
		cityCode: '',
		cityName: '',
		type: '',
		state: '',
		countryCode: 0,
		country: '',
		number: '',
		complement: '',
		phone: '',
		suframa: '',
		cnpj: '',
		operationType: '',
		useType: '',
		usagePurpose: '',
		entityIcmsStSubstitute: false,
	}

	await axios.get(`https://${account}.vtexcommercestable.com.br/api/dataentities/CL/search?_fields=entityType,activitySector,icmsTaxPayer,type,operationType,taxRegime,useType,usagePurpose,entityIcmsStSubstitute,suframa,phone&_keyword=${data.client}`, {
		headers: {
			Accept: 'application/vnd.vtex.ds.v10+json',
			'Content-Type': 'application/json',
		},
	}).then((response: AxiosResponse) => {
		if (response.data.length) {
			client.entityType = response.data[0].entityType
			client.activitySector = response.data[0].activitySector
			client.icmsTaxPayer = (response.data[0].icmsTaxPayer == 'true')
			client.issRfRate = ''
			client.pCredSN = 0
			client.type = response.data[0].type
			client.taxRegime = response.data[0].taxRegime
			client.subjectToPayrollTaxRelief = true
			client.operationType = response.data[0].operationType
			client.useType = response.data[0].useType
			client.usagePurpose = response.data[0].usagePurpose
			client.phone = response.data[0].phone
			client.suframa = response.data[0].suframa
			client.entityIcmsStSubstitute = response.data[0].entityIcmsStSubstitute
		}
	})
		.catch((error) => {
			console.error(error)
		})

	await axios.get(`https://${account}.vtexcommercestable.com.br/api/dataentities/AD/search?_fields=userId,cityCode,number,complement,street,neighborhood,postalCode,city,state,country&_keyword=${data.client}`, {
		headers: {
			Accept: 'application/vnd.vtex.ds.v10+json',
			'Content-Type': 'application/json',
		},
	}).then((response: AxiosResponse) => {
		if (response.data.length) {
			client.cityCode = response.data[0].cityCode
			client.number = response.data[0].number
			client.complement = response.data[0].complement
			client.street = response.data[0].street
			client.neighborhood = response.data[0].neighborhood
			client.zipCode = response.data[0].postalCode
			client.cityName = response.data[0].city
			client.state = response.data[0].state
			client.country = response.data[0].country
		}
	})
		.catch((error) => {
			console.error(error)
		})

	const simulationData = {
		items: [
			{
				idSku: data.skuId,
				price: null,
				quantidade: 1
			}
		],
		location: {
			zipCode: client.zipCode,
			country: client.country,
			point: []
		},
		saleschannel: 1
	}

	let respSimulation: AxiosResponse = await axios.post(`https://${account}.vtexcommercestable.com.br/api/logistics/pvt/shipping/simulation`, simulationData, {
		headers: {
			'X-VTEX-API-AppKey': VTEX_APP_KEY,
			'X-VTEX-API-AppToken': VTEX_APP_TOKEN,
		},
	})
	
	data.dockId = respSimulation.data.freightSimulated[0].slaResponseFreightSimulated.length > 0 ? respSimulation.data.freightSimulated[0].slaResponseFreightSimulated[0].dockId : 1
	data.freightPrice = respSimulation.data.freightSimulated[0].slaResponseFreightSimulated[0].listPrice

	const establishment: Establishment = {
		clientId: '',
		clientSecret: '',
		clientCompanyLocation: '',
		dockId: '',
		dockName: '',
		activitySector: '',
		icmsTaxPayer: false,
		issRfRate: '',
		pCredSN: 0,
		subjectToPayrollTaxRelief: false,
		subjectWithholdingIss: false,
		taxRegime: '',
		entityType: '',
		stateTaxId: '',
		street: '',
		neighborhood: '',
		zipCode: '',
		cityCode: '',
		cityName: '',
		state: '',
		countryCode: 0,
		country: '',
		number: '',
		complement: '',
		phone: '',
		suframa: '',
		cnpj: '',
		messageType: '',
	}

	await axios.get(`https://${account}.vtexcommercestable.com.br/api/dataentities/AE/search?_fields=clientId,clientSecret,clientCompanyLocation,dockId,dockName,activitySector,icmsTaxPayer,issRfRate,pCredSN,subjectToPayrollTaxRelief,taxRegime,entityType,stateTaxId,messageType,street,neighborhood,zipCode,cityCode,city,state,country,streetNumber,complement,phone,cnpj,suframa&_keyword=${data.dockId}`, {
		headers: {
			Accept: 'application/vnd.vtex.ds.v10+json',
			'Content-Type': 'application/json',
		},
	}).then((response: AxiosResponse) => {
		if (response.data.length) {
			establishment.clientId = response.data[0].clientId,
			establishment.clientSecret = response.data[0].clientSecret
			establishment.clientCompanyLocation = response.data[0].clientCompanyLocation
			establishment.messageType = response.data[0].messageType
			establishment.street = response.data[0].street
			establishment.taxRegime = response.data[0].taxRegime
			establishment.activitySector = response.data[0].activitySector
			establishment.icmsTaxPayer = (response.data[0].icmsTaxPayer == 'true')
			establishment.issRfRate = response.data[0].issRfRate
			establishment.pCredSN = response.data[0].pCredSN
			establishment.subjectToPayrollTaxRelief = (response.data[0].subjectToPayrollTaxRelief == 'true')
			establishment.neighborhood = response.data[0].neighborhood
			establishment.zipCode = response.data[0].zipCode,
			establishment.cityCode = response.data[0].cityCode
			establishment.cityName = response.data[0].cityName
			establishment.state = response.data[0].state
			establishment.countryCode = response.data[0].countryCode
			establishment.country = response.data[0].country
			establishment.number = response.data[0].number
			establishment.complement = response.data[0].complement
			establishment.phone = response.data[0].phone
			establishment.suframa = response.data[0].suframa
			establishment.cnpj = response.data[0].cnpj
		}
	})
		.catch((error) => {
			console.error(error)
		})

	let productList: any[] = []
	await axios.get(`https://${account}.vtexcommercestable.com.br/api/dataentities/AS/search?_where=skuId=${data.skuId}&_fields=skuId,skuInfo`)
		.then((response: AxiosResponse) => {
			if (response.data[0]) {
				const prod = JSON.parse(response.data[0]?.skuInfo)
				prod.operationType = client.operationType
				prod.useType = client.useType
				prod.usagePurpose = client.usagePurpose == null ? '' : client.usagePurpose
				prod.itemDescriptor.entityIcmsStSubstitute = client.entityIcmsStSubstitute
				prod.id = 0
				prod.lineAmount = parseFloat(data.skuPrice) / 100
				prod.lineCode = 0
				prod.numberOfItems = 1
				prod.freightAmount = data.freightPrice

				productList.push(prod)
			}
		})

	const payLoad: Object = {
		'header': {
			'transactionDate': new Date().toISOString().slice(0, 10),
			'amountCalcType': 'gross',
			'documentCode': client.docCode,
			'goods': {
				'model': '55',
			},
			'messageType': establishment.messageType,
			'eDocCreatorType': 'self',
			'eDocCreatorPerspective': true,
			'companyLocation': establishment.clientCompanyLocation,
			'operationType': '',
			'locations': {
				'establishment': {
					'activitySector': {
						'type': 'activityLine',
						'code': establishment.activitySector,
					},
					'taxesSettings': {
						'icmsTaxPayer': establishment.icmsTaxPayer,
						'issRfRate': establishment.issRfRate,
						'pCredSN': establishment.pCredSN,
						'subjectToPayrollTaxRelief': establishment.subjectToPayrollTaxRelief,
						'subjectWithholdingIss': true,
					},
					'taxRegime': establishment.taxRegime,
					'entityType': establishment.entityType,
					'stateTaxId': establishment.stateTaxId,
					'suframa': establishment.suframa ? establishment.suframa : '999999999',
					'address': {
						'street': establishment.street,
						'neighborhood': establishment.neighborhood,
						'zipcode': establishment.zipCode,
						'cityCode': establishment.cityCode,
						'cityName': establishment.cityName,
						'state': establishment.state,
						'countryCode': establishment.countryCode,
						'country': establishment.country,
						'number': establishment.number,
						'complement': establishment.complement,
						'phone': establishment.phone,
					},
					'federalTaxId': establishment.cnpj,
				},
				'entity': {
					'activitySector': {
						'type': 'activityLine',
						'code': client.activitySector,
					},
					'taxesSettings': {
						'icmsTaxPayer': client.icmsTaxPayer,
						'issRfRate': client.issRfRate,
						'pCredSN': client.pCredSN,
						'subjectToPayrollTaxRelief': client.subjectToPayrollTaxRelief,
						'subjectWithholdingIss': client.subjectWithholdingIss,
					},
					'taxRegime': client.taxRegime,
					'type': client.type,
					'entityType': client.entityType,
					'stateTaxId': client.stateTaxId,
					'suframa': client.suframa ? client.suframa : '999999999',
					'inssBasisDiscount': null,
					'inssPreviousContrib': null,
					'address': {
						'street': client.street,
						'neighborhood': client.neighborhood,
						'zipcode': client.zipCode,
						'cityCode': client.cityCode,
						'cityName': client.cityName,
						'state': client.state,
						'countryCode': client.countryCode,
						'country': client.country,
						'number': client.number,
						'complement': client.complement,
						'phone': client.phone,
					},
					'federalTaxId': client.cnpj,
				},
			},
		},
		'lines': productList,
	}

	let accessToken: string = ''

	await axios.post('https://avataxbr.avalarabrasil.com.br/v3/token', {
		grant_type: 'client_credentials',
		client_id: establishment.clientId,
		client_secret: establishment.clientSecret,
	})
		.then(async (response: AxiosResponse) => {
			accessToken = response.data.access_token
		})
		.catch((error) => {
			console.error(error)
		})

	let calculatedTaxes = await axios.post('https://avataxbr.avalarabrasil.com.br/v3/calculations', JSON.stringify(payLoad), {
		headers: {
			Authorization: `Bearer ${accessToken}`,
			'Content-Type': 'application/json',
			Accept: 'application/json',
		},
	})

	const initLogData = {
		orderId: data.dockId,
		data: new Date(),
		url: currentUrl,
		email: data.client,
		payLoad: JSON.stringify(payLoad),
		client: client,
		establishment: establishment,
		freightSimulation: data.freightPrice,
		avalaraReturn: '',
		pdpReturn: calculatedTaxes.data,
		type: 'Página de Produto'
	}

	axios.post(`https://${account}.vtexcommercestable.com.br/api/dataentities/AL/documents`, initLogData, {
		headers: {
			Accept: 'application/vnd.vtex.ds.v10+json',
			'Content-Type': 'application/json',
			'x-vtex-api-appKey': VTEX_APP_KEY,
			'x-vtex-api-appToken': VTEX_APP_TOKEN
		},
	}).then(() => {
		console.log('Log saved!')
	})
	.catch((error) => {
		console.error(error)
	})
	
	ctx.body = calculatedTaxes.data.lines

	ctx.set('Cache-control', 'no-cache')
	ctx.set('Access-Control-Allow-Origin', '*')
	ctx.set('Content-Type', 'application/json')

	await next()

}
